package LoginExerciseCarolina;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Exercise8login  .
 */
@WebServlet("/PasswordEx")
public class PasswordEx extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PasswordEx() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/WEB-INF/PasswordEx1.html").forward(request, response);;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String[] legalPasswords = new String[4];
		legalPasswords[0] = "caro";
		legalPasswords[1] = "carolina";
		legalPasswords[2] = "costin";
		legalPasswords[3] = "Abcd";
		
        String password = request.getParameter("password");

        response.setContentType("text/html");
        PrintWriter printWriter = response.getWriter();
        
        if(Arrays.asList(legalPasswords).contains(password)) {
        printWriter.print("<html>");
        printWriter.print("<body>");
        printWriter.print("Check of Your Password");
        printWriter.print("<p>" + "login ok" + "</p>");
        printWriter.print("</body>");
        printWriter.print("</html>");}
        else {
        printWriter.print("<html>");
        printWriter.print("<body>");
        printWriter.print("Check of Your Password");
        printWriter.print("<p>" + "login ko" + "</p>");
        printWriter.print("</body>");
        printWriter.print("</html>");
        }
        printWriter.close();

	}

}

